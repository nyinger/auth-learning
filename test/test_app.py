import time, socket, subprocess, os
import requests

def setup():
    global server
    print()
    
    server = subprocess.Popen(['python', '-m', 'waitress', '--port', '8000', 'app:app'])
    wait_for_port(8000)

def teardown():
    server.terminate()

def wait_for_port(port):
    start_time = time.perf_counter()
    while True:
        try:
            with socket.create_connection(('localhost', port), timeout=30):
                break
        except OSError as e:
            time.sleep(0.01)
            if time.perf_counter() - start_time >= 5:
                raise TimeoutError('Connecting to localhost:{} timed out'.format(port)) from e

def test_landing_page_exists():
    res = requests.get('http://localhost:8000/')
    assert res.status_code == 200

def test_login_guest():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    assert res.json()['user'] == 'some-username'
    assert res.json()['auth_type'] == 'guest'
    assert res.json()['token'][:7] == 'Bearer '
    assert len(res.json()['token']) > 32
    assert get_active_logins() == ['some-username']

def test_login_guest_too_short():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': '',
    })
    assert res.status_code == 400
    assert get_active_logins() == []

def test_login_guest_too_long():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 65*'a',
    })
    assert res.status_code == 400
    assert get_active_logins() == []
    
def test_login_guest_already_taken():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    assert get_active_logins() == ['some-username']
    
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 409
    assert get_active_logins() == ['some-username']

def test_any_tests_at_all_for_the_other_logins() :
    raise NotImplementedError("It's just a demo")

def test_logout():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    assert get_active_logins() == ['some-username']
    
    res = requests.post('http://localhost:8000/logout', headers={
        'Authorization': res.json()['token'],
    })
    assert res.status_code == 200
    assert get_active_logins() == []

def test_logout_bad_token():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    assert get_active_logins() == ['some-username']
    
    res = requests.post('http://localhost:8000/logout', headers={
        'Authorization': res.json()['token'][:-2],
    })
    assert res.status_code == 403
    assert get_active_logins() == ['some-username']

def test_status():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    
    res = requests.post('http://localhost:8000/status', params={
        'status': 'a-status',
    }, headers={
        'Authorization': res.json()['token'],
    })
    assert res.status_code == 200
    
    active_logins = requests.get('http://localhost:8000/active-logins').json()
    assert len(active_logins) == 1
    assert active_logins[0]['status'] == 'a-status'

def test_status_too_long():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    
    res = requests.post('http://localhost:8000/status', params={
        'status': 65*'a',
    }, headers={
        'Authorization': res.json()['token'],
    })
    assert res.status_code == 400
    
    active_logins = requests.get('http://localhost:8000/active-logins').json()
    assert len(active_logins) == 1
    assert active_logins[0]['status'] == ''

def test_status_bad_token():
    res = requests.post('http://localhost:8000/login-guest', data={
        'user': 'some-username',
    })
    assert res.status_code == 200
    
    res = requests.post('http://localhost:8000/status', params={
        'status': 'a-status',
    }, headers={
        'Authorization': res.json()['token'][:-2],
    })
    assert res.status_code == 403
    
    active_logins = requests.get('http://localhost:8000/active-logins').json()
    assert len(active_logins) == 1
    assert active_logins[0]['status'] == ''

def get_active_logins():
    return [login['user'] for login in requests.get('http://localhost:8000/active-logins').json()]
