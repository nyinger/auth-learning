import logging, logging.config, collections, secrets, json, pathlib, os, base64, hashlib
import requests

from flask import Flask, request, session, send_file, make_response, redirect

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s.%(msecs)03d, %(levelname)s, %(message)s',
            'datefmt': '%Y-%m-%dT%H:%M:%S',
        },
    },
    'handlers': {
        'stdout': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'formatter': 'default',
            'level': 'INFO',
        },
        'logfile': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logger.log',
            'formatter': 'default',
            'level': 'INFO',
            'maxBytes': 2**20,
            'backupCount': 2,
        },
    },
    'root': {
        'handlers': ['stdout', 'logfile'],
        'level': 'INFO',
    },
})

password_file = pathlib.Path(__file__).parent / 'passwords.json'
if not password_file.exists():
    with open(password_file, 'w') as f:
        f.write('{}')

ActiveLogin = collections.namedtuple('ActiveLogin', ['user', 'auth_type', 'status'])

active_logins = {}

app = Flask(__name__)

@app.after_request
def after_request(response):
    # Don't log raw request data in production, since it contains passwords. Query strings are okay since secrets
    # shouldn't be sent in query strings anyway
    
    if request.query_string: app.logger.info(f'{request.method} {request.path} query string: {request.query_string}')
    
    app.logger.info(f'Completed {request.method} {request.path} - {response.status}')
    return response

@app.route('/')
def get_():
    return send_file('index.html')

@app.route('/login-guest', methods=['POST'])
def get_login_guest():
    # Length limit prevents a possible DoS
    if not 1 <= len(request.form['user']) <= 64:
        return 'Username must be between 1 and 64 characters (inclusive)', 400
    
    with open(password_file) as f:
        passwords = json.load(f)
    
    if request.form['user'] in passwords:
        return f"User \"{request.form['user']}\" already registered", 409
    
    for active_login in active_logins.values():
        if active_login.user == request.form['user'] and active_login.auth_type == 'guest':
            return f"User \"{request.form['user']}\" already in use as guest account", 409
    
    token = 'Bearer ' + secrets.token_urlsafe(32)
    active_logins[token] = ActiveLogin(request.form['user'], 'guest', '')
    return json.dumps(dict(active_logins[token]._asdict(), token=token)), 200

@app.route('/login-password', methods=['POST'])
def get_login_password():
    # Length limit prevents a possible DoS
    if not 1 <= len(request.form['user']) <= 64:
        return 'Username must be between 1 and 64 characters (inclusive)', 400
    if not 6 <= len(request.form['pass']) <= 64:
        return 'Password must be between 6 and 64 characters (inclusive)', 400
    
    with open(password_file) as f:
        passwords = json.load(f)
    
    if 'register' in request.form:
        if request.form['user'] in passwords:
            return f"User \"{request.form['user']}\" already registered", 409
        
        for active_login in active_logins.values():
            if active_login.user == request.form['user'] and active_login.auth_type == 'guest':
                return f"User \"{request.form['user']}\" already in use as guest account", 409
    
        # Salts don't need to long, just enough to add some randomness to passwords
        salt = os.urandom(6)
        # Python docs advised "As of 2013, at least 100,000 iterations of SHA-256 are suggested", so do a bit more
        # than that
        hash = hashlib.pbkdf2_hmac('SHA512', bytes(request.form['pass'], 'utf8'), salt, 200000)
        
        passwords[request.form['user']] = {
            'salt': str(base64.b64encode(salt), 'ascii'),
            'hash': str(base64.b64encode(hash), 'ascii'),
        }
        
        print(passwords)
        with open(password_file, 'w') as f:
            passwords = json.dump(passwords, f, indent=2)
    else:
        if request.form['user'] not in passwords:
            return f"User \"{request.form['user']}\" not registered", 409
        
        salt = base64.b64decode(passwords[request.form['user']]['salt'])
        expected_hash = base64.b64decode(passwords[request.form['user']]['hash'])
        actual_hash = hashlib.pbkdf2_hmac('SHA512', bytes(request.form['pass'], 'utf8'), salt, 200000)
        
        if actual_hash != expected_hash:
            # Security people will tell you not to give descriptive error messages. You should do it anyway, because
            # it makes life easier for users
            return f'Password does not match', 409
    
    token = 'Bearer ' + secrets.token_urlsafe(32)
    active_logins[token] = ActiveLogin(request.form['user'], 'password', '')
    return json.dumps(dict(active_logins[token]._asdict(), token=token)), 200

@app.route('/active-logins', methods=['GET'])
def get_active_logins():
    return json.dumps([active_login._asdict() for active_login in active_logins.values()]), 200

@app.route('/logout', methods=['POST'])
def post_logout():
    if request.headers['Authorization'] not in active_logins:
        return 'Token not recognized', 403
    
    del active_logins[request.headers['Authorization']]
    return '', 200

@app.route('/status', methods=['POST'])
def post_status():
    # Length limit prevents a possible DoS
    if not len(request.args['status']) <= 64:
        return 'Status must be no more than 64 characters', 400
    
    if request.headers['Authorization'] not in active_logins:
        return 'Token not recognized', 403
    
    active_logins[request.headers['Authorization']] = ActiveLogin(
        active_logins[request.headers['Authorization']].user,
        active_logins[request.headers['Authorization']].auth_type,
        request.args['status'],
    )
    return '', 200

github_random_states = {}

@app.route('/github-random-state', methods=['POST'])
def post_github_random_state():
    if 'state' not in request.args:
        return 'Query string paramter "state" required', 400
    
    github_random_states[request.args['state']] = None
    return '', 200

# This is a stability risk if GitHub responds slowly - in production this funciton should be in its own thread
@app.route('/github-callback', methods=['GET'])
def get_github_callback():
    if request.args['state'] not in github_random_states:
        return 'GitHub random state mismatch', 403
    
    github_token_response = requests.post('https://github.com/login/oauth/access_token', params={
        # Client ID and secret are both supplied by GitHub when registering an app with them
        'client_id': 'cdafe67d90d3eda58e4d',
        # This would normally be stored outside the repo, but is included for demo purposes
        'client_secret': '4f1355ef15c81b89ad339074ea89186ad8bcb9d3',
        'code': request.args['code'],
    }, headers={
        'Accept': 'application/json',
    })
    
    if github_token_response.status_code != 200:
        return 'Login rejected by GitHub', 403
    
    github_user_response = requests.get('https://api.github.com/user', headers={
        'Authorization': 'token ' + github_token_response.json()['access_token'],
    })
    
    if github_user_response.status_code != 200:
        print(github_user_response.status_code)
        print(github_user_response.content)
        return 'Unable to get username from GitHub', 500
    
    github_random_states[request.args['state']] = github_user_response.json()['login']
    return redirect('/')

@app.route('/github-exchage-state', methods=['POST'])
def post_github_exchange_state():
    if 'state' not in request.args:
        return 'Query string paramter "state" required', 400
    
    if request.args['state'] not in github_random_states:
        return 'GitHub random state mismatch', 403
    
    username = github_random_states[request.args['state']]
    del github_random_states[request.args['state']]
    
    if username is None:
        return 'GitHub authentication not done', 403
    
    token = 'Bearer ' + secrets.token_urlsafe(32)
    active_logins[token] = ActiveLogin(username, 'github', '')
    return json.dumps(dict(active_logins[token]._asdict(), token=token)), 200
